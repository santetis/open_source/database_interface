import 'dart:async';

/// Is an interface that all collection need to implement
abstract class TableInterface {
  Future<dynamic> query(String query);
}

/// Is an interface that all no sql database need to implement
abstract class SqlDatabaseInterface {
  bool get isOpen;

  Future<void> close();

  TableInterface table(String name);

  Future<void> open();
}
