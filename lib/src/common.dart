import 'package:conn_pool/conn_pool.dart';
import 'package:quiver/core.dart';

class DatabaseException implements Exception {
  final String message;

  DatabaseException(this.message);
}

/// Represent the id in the database
class DatabaseId {
  final String id;

  DatabaseId(this.id);

  @override
  int get hashCode => hashObjects(<String>[id]);

  @override
  bool operator ==(Object other) {
    if (other is DatabaseId) {
      return other.id == id;
    } else if (other is String) {
      return other == id;
    }
    return false;
  }

  @override
  String toString() => 'DatabaseIdInterface("$id")';
}

/// Is an interface that all no sql database need to implement
abstract class DatabaseInterface<T> {
  bool get isOpen;

  Future<void> close();

  T collection(String name);

  Future<void> open();
}

/// A function that return a compatible no sql database
typedef DatabaseInterface<T> FactoryDatabase<T>();

class DatabaseManager<T extends DatabaseInterface>
    implements ConnectionManager<T> {
  DatabaseManager(this.factoryDatabase);

  final FactoryDatabase factoryDatabase;

  @override
  Future<void> close(T connection) => connection.close();

  @override
  Future<T> open() async {
    final database = factoryDatabase();
    await database.open();
    return database;
  }
}

/// A generic database pool for no sql database
class DatabasePool<T extends DatabaseInterface> {
  DatabasePool.fromManager(DatabaseManager<T> manager,
      {int minPoolSize = 10, int maxPoolSize = 10})
      : pool = SharedPool(manager, minSize: minPoolSize, maxSize: maxPoolSize);

  final Pool<T> pool;
}
