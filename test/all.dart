import 'common_test.dart' as common_test;
import 'no_sql_database_interface_test.dart' as no_sql_database_interface_test;
import 'sql_database_interface_test.dart' as sql_database_interface_test;

void main() {
  common_test.main();
  no_sql_database_interface_test.main();
  sql_database_interface_test.main();
}
