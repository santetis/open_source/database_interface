[![pipeline status](https://gitlab.com/santetis/open-source/database_interface/badges/master/pipeline.svg)](https://gitlab.com/santetis/open-source/database_interface/commits/master)
[![coverage report](https://gitlab.com/santetis/open-source/database_interface/badges/master/coverage.svg)](https://gitlab.com/santetis/open-source/database_interface/commits/master)

#   Use this package to write compatible no sql database

You will need to implement two class in order to have a compatible no sql database

## The NoSqlDatabaseInterface
For the Database we provide a [NoSqlDatabaseInterface](https://gitlab.com/santetis/open-source/database_interface/blob/master/lib/src/no_sql_interfaces.dart#L58) that allow you to do do action at differents step of the life of your database.

- when a user open your database with the `open()` method
- when a user close your database with the `close()` method
- when a user want to open a collection with the `collection('my_collection')` method

You can find a fake no sql database [here](https://gitlab.com/santetis/open-source/database_interface/blob/master/test/src/fake_no_sql_database.dart#L28)
by it self this database does nothing. It just implement all the necessary method to be a valid database. 

## The CollectionInterface
For the Collection we provide a [CollectionInterface](https://gitlab.com/santetis/open-source/database_interface/blob/master/lib/src/no_sql_interfaces.dart#L7) that allow you to do things on your collection like insert entry, update entry, etc

You can find a fake no sql collection [here](https://gitlab.com/santetis/open-source/database_interface/blob/master/test/src/fake_no_sql_database.dart#L5) that interface does nothing but you get the idea.

# How to deal with id
Database has specific id to handle that we provide a class that must be used by the end user so it never deal with a database specific id.
This class is [DatabaseId](https://gitlab.com/santetis/open-source/database_interface/blob/master/lib/src/no_sql_interfaces.dart#L35) just wrap a String where you can store the id of the specific database.


