OBS_PORT=9299
echo "Collecting coverage on port $OBS_PORT..."
#   Start test in one VM
dart \
    --enable-vm-service=$OBS_PORT \
    --pause-isolates-on-exit \
    test/all.dart &

# Run the coverage collector to generate the JSON coverage report.
collect_coverage \
    --uri=http://127.0.0.1:$OBS_PORT/ \
    --out=var/coverage.json \
    --wait-paused \
    --resume-isolates \
    -t 15
OUT=$?
if [ $OUT -ne 0 ];then
    echo "Fail to collect coverage stop."
    exit $OUT
fi

echo "Generating LCOV report..."
format_coverage \
    --lcov \
    --in=var/coverage.json \
    --out=var/lcov.info \
    --packages=.packages \
    --report-on=lib
OUT=$?
if [ $OUT -ne 0 ];then
   echo "Fail to format coverage stop."
   exit $OUT
fi

genhtml -o coverage var/lcov.info
